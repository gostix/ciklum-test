package com.library.service;

import com.library.model.Author;
import com.library.repository.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuthorServiceImpl implements AuthorService
{
    private final AuthorRepository authorRepository;

    @Autowired
    public AuthorServiceImpl(AuthorRepository authorRepository)
    {
        this.authorRepository = authorRepository;
    }

    @Override
    public Author save(Author author)
    {
        return authorRepository.save(author);
    }

    @Override
    public Author findAuthor(String firstName, String lastName)
    {
        return authorRepository.findAuthor(firstName, lastName);
    }

    @Override
    public List<Author> getAuthors(Long bookId)
    {
        return authorRepository.findAuthors(bookId);
    }
}
