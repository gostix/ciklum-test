package com.library.service;

import com.library.model.Author;

import java.util.List;

public interface AuthorService
{
    Author save(Author author);
    Author findAuthor(String firstName, String lastName);
    List<Author> getAuthors(Long bookId);
}
