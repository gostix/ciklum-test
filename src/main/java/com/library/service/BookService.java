package com.library.service;

import com.library.model.Book;

import java.util.List;

public interface BookService
{
    void save(Book book);
    List<Book> getBooks(Long authorId);
}
