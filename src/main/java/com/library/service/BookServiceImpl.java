package com.library.service;

import com.library.model.Author;
import com.library.model.Book;
import com.library.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class BookServiceImpl implements BookService
{
    private final BookRepository bookRepository;
    private final AuthorService authorService;

    @Autowired
    public BookServiceImpl(BookRepository bookRepository, AuthorService authorService)
    {
        this.bookRepository = bookRepository;
        this.authorService = authorService;
    }

    @Override
    @Transactional
    public void save(Book book)
    {
        List<Author> authors = book.getAuthors();
        for (int i = 0; i < authors.size() ; i++)
        {
            Author author = authors.get(i);
            Author checkedAuthor = authorService.findAuthor(author.getFirstName(), author.getLastName());
            if(checkedAuthor != null)
            {
                authors.set(i, checkedAuthor);
            }
            else
            {
                Author savedAuthor = authorService.save(author);
                authorService.save(savedAuthor);
                authors.set(i, savedAuthor);
            }
        }
        bookRepository.save(book);
    }

    @Override
    public List<Book> getBooks(Long authorId)
    {
        return bookRepository.findBooks(authorId);
    }
}
