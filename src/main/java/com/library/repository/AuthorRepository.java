package com.library.repository;

import com.library.model.Author;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AuthorRepository extends JpaRepository<Author, Long>
{
    @Query("SELECT a FROM Author a join a.books b WHERE b.id = :bookId")
    List<Author> findAuthors(@Param("bookId") Long bookId);

    @Query("SELECT a FROM Author a WHERE a.firstName = :firstName AND a.lastName = :lastName")
    Author findAuthor(@Param("firstName") String firstName, @Param("lastName") String lastName);
}
