package com.library;

import com.library.model.Author;
import com.library.model.Book;
import com.library.service.AuthorService;
import com.library.service.BookService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class ApplicationRunner
{

    private static final Logger LOG = LoggerFactory.getLogger(ApplicationRunner.class);

    public static void main(String[] args) {
        SpringApplication.run(ApplicationRunner.class);
    }

    @Bean
    public CommandLineRunner demo(AuthorService authorService, BookService bookService) {
        return (args) -> {

            Author author1 = new Author("name1", "last1");
            Author author2 = new Author("name2", "last2");
            Author author3 = new Author("name3", "last3");

            List<Author> bookAuthors1 = new ArrayList<>();

            bookAuthors1.add(author1);
            bookAuthors1.add(author2);
            bookAuthors1.add(author3);
            Book book1 = new Book("someTitle1", bookAuthors1);

            List<Author> bookAuthors2 = new ArrayList<>();
            bookAuthors2.add(author1);
            bookAuthors2.add(author2);
            Book book2 = new Book("someTitle2", bookAuthors2);
            bookService.save(book1);
            bookService.save(book2);

            LOG.info("-------------------------------");

            long bookId = book1.getId();
            List<Author> authors = authorService.getAuthors(bookId);
            LOG.info("Authors by book id:" + bookId + " = " + authors);

            LOG.info("-------------------------------");

            LOG.info("-------------------------------");

            long authorId = author1.getId();
            List<Book> books = bookService.getBooks(authorId);
            LOG.info("Books by author id:" + authorId + " = " + books);

            LOG.info("-------------------------------");
        };
    }
}
