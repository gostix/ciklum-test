package com.library.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@ToString(exclude = "authors")
@Getter
@NoArgsConstructor
@Entity
public class Book
{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String title;

    public Book(String title, List<Author> authors)
    {
        this.title = title;
        this.authors = authors;
    }

    @ManyToMany(cascade = CascadeType.MERGE)
    @JoinTable(name = "author_book", joinColumns = @JoinColumn(name = "book_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "author_id", referencedColumnName = "id"))
    List<Author> authors;
}
